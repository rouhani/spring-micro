# Reaktive mikrotjenester med Spring


Innholdsfortegnelse
===================

* [Kilder](#kilder)
* [Introduksjon](#introduksjon)
* [Eksempelprosjekt](#eksempelprosjekt)
* [Spring Boot](#spring-boot)
* [Flux og Mono](#flux-og-mono)
* [Applikasjonen](#applikasjonen)

Kilder
======

Prosjektet er bygd på eksempler fra disse kildene, som også er fine
praktiske artikler om Spring, microservices og reactive streams:

-   [https://medium.com/omarelgabrys-blog/microservices-with-spring-boot-intro-to-microservices-part-1-c0d24cd422c3](https://medium.com/omarelgabrys-blog/microservices-with-spring-boot-intro-to-microservices-part-1-c0d24cd422c3)

-   [https://medium.com/omarelgabrys-blog/microservices-with-spring-boot-creating-our-microserivces-gateway-part-2-31f8aa6b215b](https://medium.com/omarelgabrys-blog/microservices-with-spring-boot-creating-our-microserivces-gateway-part-2-31f8aa6b215b)

-   [https://www.baeldung.com/reactor-core](https://www.baeldung.com/reactor-core)

-   [https://dzone.com/articles/reactive-microservices-with-spring-webflux-and-spr](https://dzone.com/articles/reactive-microservices-with-spring-webflux-and-spr)

-   [https://dzone.com/articles/event-streaming-using-spring-webflux](https://dzone.com/articles/event-streaming-using-spring-webflux)

Introduksjon
============

*Spring* ([https://spring.io/](https://spring.io/)) er et
rammeverk for å bygge store applikasjoner på Java-platform. Med versjon
5 introduserer de nå også en ***reactive*** platform. Det betyr at vi
kan lage microservices ved å kjøre hver REST-tjeneste i egen prosess, og
at disse tjenestene kommuniserer med hverandre *asynkront*.

Den asynkrone kommuniksasjonen er implementert i *Spring Reactive Core*
som er bygd på *Reactive Streams*:
[http://www.reactive-streams.org/](http://www.reactive-streams.org/).

Eksempelprosjekt
================

Denne prosjektet gjør ikke noe
fornuftig, annet enn å utføre kall mellom hverandre som illustrerer de
tekniske mulighetene med reaktive microservices

Det inneholder fire komponenter, hvorav to er egne microservices
(Service1 og Service2).

Prosjektet er et maven-prosjekt, med en *pom.xml* for hele prosjektet
som ligger på rotkatalogen, og en pom.xml for hvert av del-prosjektene
som ligger i underkatalogene.

Bruk *pom.xml* på rotkatalogen som prosjektfil, og åpne denne i for
eksempel IntelliJ IDEA:

![](media/image1.png)

Tjenestene kan startes hver for seg ved å kjøre \*Application-klassene.
Dette kan gjøres direkte fra IDE'en, så lenge prosjektene er åpnet via
*pom.xml*-filene.

Arkitekturen til systemet er som følger:

![](media/image2.png)

*Discovery*-serveren (Eureka) er en navnetjeneste. Da kan tjenestene
registrere seg selv med navn, host og port, så kan andre tjenester kun
bruke navnet når de skal kontakte denne. Det gjør at oppsettet blir mye
mer fleksibelt, og at tjenestene kan bytte host uten problemer.
Tjenestene «pinger» også discovery-serveren slik at denne alltid vet om
tjenestene er oppe.

*Gateway*-serveren er en «router» som gjør at web-klienter kan nå
tjenestene via denne for å slippe å adressere tjenestene direkte.
Gateway bruker igjen Discovery-tjenesten for å finne tjenestene.

*Service-1* og *Service-2* er reaktive mikrotjenester.

Spring Boot
===========

*Spring Boot* er en del av Spring som handler om å få opp applikasjoner
veldig raskt, med et minimum av konfigurasjon. I vårt tilfelle betyr det
at vi enkelt kan opprette web-servere med Spring-komponenter. Når vi nå
skal lage microservices kan vi bruke Spring Boot til å enkelt opprette
en prosess per microservice. Ved å kjøre følgende kode vil en
Tomcat-server (default) fyres opp. Det må også følge med en enkel
konfigurasjonsfil.

```java
package service1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
@EnableEurekaClient
public class Service1Application {

   public static void main(String[] args) {
      SpringApplication.run(Service1Application.class, args);
   }

   @Bean
   @LoadBalanced
   public WebClient.Builder loadBalancedWebClientBuilder() {
      return WebClient.builder();
   }
   
}
```

```java
@RestController
public class Service1Controller {

   @GetMapping()
   public Flux<Service1Bean> getAll() {
      return Flux.just(new Service1Bean("1"), new Service1Bean("2"));
   }
}
```

Vi bruker annotasjoner for å spesifisere tjenesten, ganske likt slik man
gjøre det med JAX-RS. *\@RestController* lager en REST-tjeneste, mens
*\@GetMapping* (eller *\@PostMapping* osc) lager et endepunkt. Her kan
vi også spesifisere path og MediaType, f.eks:

```java
@GetMapping(value = "/messages", produces = MediaType.APPLICATION_JSON)
```

Flux og Mono
============

*Spring Reactive Core* bruker datatypene *Flux* og *Mono*.

*Flux* er en *stream* som kan sende flere elementer, mens *Mono* er en
*stream* som kan sende 1 element. Dette er godt forklart her:
[https://www.baeldung.com/reactor-core](https://www.baeldung.com/reactor-core).

Følgende metodesignatur sier at denne metoden skal returnere flere
Service1Bean-objekter, ETTER HVERT:

```java
@GetMapping()
public Flux<Service2Bean> getAll() {
```

«Etter hvert» kan bety «når alt er klart» (som en Future) eller «etter
hvert som de kommer» (Stream). Hvis de skal leveres som en strøm må vi
bruke en egen *MediaType* på endepunktet:

```java
@GetMapping(value = "/messages", produces = MediaType. TEXT_EVENT_STREAM_VALUE)
```

Applikasjonen
=============

Åpne prosjektet i IDE'en du bruker og start alle 4 tjenestene;
Discovery, Gateway, Service2 og Service1, i den rekkefølgen.

Prøv så å åpne følgende i nettleseren:

-   [http://localhost:8090/service2](http://localhost:8090/service2)

-   [http://localhost:8090/service1](http://localhost:8090/service1)

-   [http://localhost:8090/service2/messages](http://localhost:8090/service2/messages)

Sørg for at du forstår hva som skjer:

-   Når du åpner
    [http://localhost:8090/service2](http://localhost:8090/service2)
    vil tjenesten først gå til gateway'en (localhost:8090) med navnet på
    tjenesten, service2.

-   Gateway'en vil videresende til service2 som er på
    [http://localhost:2223](http://localhost:2223)

-   Der vil endepunktet getAll() kjøres

-   Den sender så en *asynkron* POST til service1 - den
    venter altså ikke på svar

-   Så kaller den getAll på service1, men venter ikke på svar nå heller,
    men sier at når vi får svar, så skal denne mappes til egen datatype,
    før den sendes tilbake til klienten.

-   Heller ikke nå ventes det på svar, vi returnerer altså fra metoden
    før vi har fått svar fra service1. Klienten vil likevel få svaret
    når det er klart.

I tillegg opprettes det en *stream* som kan sees på
[http://localhost:8090/service2/messages](http://localhost:8090/service2/messages):

-   Streamen begynner å sende når Service2Controller er klar til bruk
    via en ApplicationReady-event. Det brukes en timer som sender en
    melding hvert 5 sekund.

-   Service1 begynner å abonnere på denne når Service1Controller er klar
    ved å kalle på
    [http://localhost:8090/service2/messages](http://localhost:8090/service2/messages).

-   Service1 logger alle disse meldingene til konsollet.

Noen av tjenestene har lagt inn pauser på 2 sekunder som illustrerer det
asynkrone aspektet. Se på loggen til Service2 (og Service1) etter du har
kalt denne og observer tidspunktene loggmeldingene kom på.
